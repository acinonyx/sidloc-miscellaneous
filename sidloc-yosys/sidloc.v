module top (input clk, input rstn, output [1:0] leds);
	blink #(.PERIOD(13000000)) b0(.clk(clk), .rstn(rstn), .led(leds[0]));
	blink #(.PERIOD(52000000)) b1(.clk(clk), .rstn(rstn), .led(leds[1]));
endmodule

module blink
	#(parameter PERIOD = 16384)(input clk, input rstn, output reg led);
	
	reg [63:0] counter;
	
	always@(posedge clk) begin
		if(!rstn) begin
			led <= 1;
			counter <= 0;
		end
		else begin
			if(counter == PERIOD) begin
				led <= ~led;
				counter <= 0;
			end
			else begin
				counter <= counter + 1;
			end
		end
	end
endmodule

	
